function [hb, he] = plot_barerrors(barval, barerr)
% [hb, he] = plot_barerrors(barval, barerr)
%
% This function plots bar and errorbar together. It is tested for vectors,
% to be tested for matrix (multiplebars).
% barval and barerr must have the same size

    if isequal(size(barval), size(barerr)) == false
        error('chk:size', 'barval and barerr must have the same size')
    end
    
    hold on;
    he = errorbar(barval, barerr, '.k');
    hb = bar(barval);
    hold off;

end