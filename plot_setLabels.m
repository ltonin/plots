function [handles] = plot_setLabels(handles, labelX, labelY)
% PLOT_SETLABELS  Set the X and Y labels
%
%           plot_setLabels(handles, labelX, labelY)
%                         
%                           - handles       the link to the figure (e.g. gca)
%                           - labelsX        
%                           - labelsY   
%


    if nargin < 2
        labelX = '';
        labelY = '';
    elseif nargin <3
        labelY = '';
    end


    for hIdx = 1:length(handles)
        
        if ~(isempty(labelX))
            xlabel(handles(hIdx), labelX);
        end
        
        if ~(isempty(labelY))
            ylabel(handles(hIdx), labelY);
        end
    
    end

end
