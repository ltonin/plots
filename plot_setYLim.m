function [handles] = plot_setYLim(handles, values)
% PLOT_SETYLIM   This function set the Ylim of the argument handles to the same
%           values.
%
%           plot_setYLim(handles, values)
%                                   - handles:  the figures axis handles,
%                                               can be a vector.
%                                   - values:   min and max value in the
%                                               format [min max]  

    if ~(length(values)== 2)
        error('setAx:chk', 'The values input must be in the format [min max]');
    end
    
    if values(2) < values(1)
        error('setAx:chk', 'The values input must be in the format [min max]');
    end

    
    for hIdx = 1:length(handles)     
        ylim(handles(hIdx), values);
    end

end
