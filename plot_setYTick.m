function [handles] = plot_setYTick(handles, tick_labels, tick) 
% PLOT_SETYTICK  Set the ticks and the labels of given plot for X axis. The
%           number of ticks and the number of labels must be the same.
%
%           plot_setYTick(handles, tick_labels, tick)
%                         
%                           - handles       the link to the figure (e.g. gca)
%                           - tick          the position in the x-axis
%                           - tick_labels   the labels for the ticks
%

    for hIdx = 1:length(handles)
    
        if nargin == 2
        
            tick = get(handles(hIdx), 'YTick');
        end
  


        if (length(tick) ~= length(tick_labels))
            error('ytick:chk', 'Number of ticks and number of labels must be the same');
        end

        labels = cell(length(tick_labels));

        if iscell(tick_labels)
            labels = tick_labels;
        else
            for lbIdx = 1:length(tick_labels)
                labels{lbIdx} = num2str(tick_labels(lbIdx));
            end
        end

   
        set(handles(hIdx), 'YTick', tick);
        set(handles(hIdx), 'YTickLabel', labels); 
        
    end
    
    
end
