function plot_errp(t, correct, error)

    hold on;
    plot(t, error, 'r');
    plot(t, correct, 'b');
    plot(t, error - correct, 'k', 'LineWidth', 2);
    hold off;
    
    xlim([t(1) t(end)]);
    grid on;

end