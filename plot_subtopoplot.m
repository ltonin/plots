function [f, subh] = plot_subtopoplot(data, chanlocs, varargin)
% [f, subh] = plot_subtopoplot(data, chanlocs, varargin)
%
% This function creates subplots of topoplots. Input arguments are similar
% to the ones used in topoplots (see topoplot from eeglab) with some 
% optional values.
% 
%   Input:
%
%       data            Input data [subplots x channels]
%       chanlocs        Channel locations (as in topoplot function)
%
%   Optional pairs in addition to topoplots arguments:
%
%       'ncolumns'      Maximum number of columns in the subplots 
%                       [Default: 6]
%       'title'         General figure title [Default: '']
%       'subtitles'     Titles of subplots. If the argument is a string,
%                       the same title is repeated for each subplot. If the
%                       argument is a cell array with length equal to the
%                       number of subplots, each elements of the cell array
%                       is used as a title for the subplots. [Deafult: '']
%       'xlabels'       X labels for the subplots. If the argument is a 
%                       string, the same label is repeated for each 
%                       subplot. If the argument is a cell array with 
%                       length equal to the number of subplots, each 
%                       elements of the cell array is used as a label for 
%                       the subplots. Labels are positioned at the bottom 
%                       of each subplot [Deafult: '']
%       'ylabels'       Y labels for the subplots. If the argument is a 
%                       string, the same label is repeated for each 
%                       subplot. If the argument is a cell array with 
%                       length equal to the number of subplots, each 
%                       elements of the cell array is used as a label for 
%                       the subplots. Labels are positioned at the left 
%                       of each subplot [Deafult: '']
%       
% SEE ALSO: topoplot, plot_add_text
    
    % Getting input arguments
    anames   = {'ncolumns', 'title', 'subtitles', 'xlabels', 'ylabels'}; 
    adefault = {6,             [],          [],        [],        []};
    
    [~, ~, ncolumns, figtitle, subtitles, xlabels, ylabels, others] = util_getargs(anames, adefault, varargin{:});
   
    nsubplots = size(data, 1);
    
    % Check subtitles dimensions
    if isempty(subtitles) == false
        if ischar(subtitles) == true
            subtitles = cellstr(repmat(subtitles, [nsubplots, 1]));
        elseif iscell(subtitles) == true
            if size(subtitles, 2) ~= nsubplots
                error('chk:subt', 'Provided cell array for subtitles must have the same dimensions of data');
            end   
        end
    end
    
    % Check xlabels dimensions
    if isempty(xlabels) == false
        if ischar(xlabels) == true
            xlabels = cellstr(repmat(xlabels, [nsubplots, 1]));
        elseif iscell(xlabels) == true
            if size(xlabels, 2) ~= nsubplots
                error('chk:subt', 'Provided cell array for xlabels must have the same dimensions of data');
            end   
        end
    end
    
    % Check ylabels dimensions
    if isempty(ylabels) == false
        if ischar(ylabels) == true
            ylabels = cellstr(repmat(ylabels, [nsubplots, 1]));
        elseif iscell(ylabels) == true
            if size(ylabels, 2) ~= nsubplots
                error('chk:subt', 'Provided cell array for ylabels must have the same dimensions of data');
            end   
        end
    end
    
    
    % Check the number of columns requested and the number of subplots
    if nsubplots < ncolumns
        ncolumns = nsubplots;
    end
    
    nrows = ceil(nsubplots/ncolumns);
    
    subh = zeros(nsubplots, 1);
    
    % Plotting subplots
    for sId = 1:nsubplots
       
        subplot(nrows, ncolumns, sId);
        
        cdata = data(sId, :);
        
        topoplot(cdata, chanlocs, others{:});
        axis image;
        subh(sId) = gca;
        
        % Adding title to subplot (if requested)
        if isempty(subtitles) == false
            title(subtitles{sId});
        end
        
        % Adding xlabel to subplot (if requested)
        if isempty(xlabels) == false
            plot_add_text(subh(sId), xlabels{sId}, 'bottom');
        end
        
        % Adding ylabel to subplot (if requested)
        if isempty(ylabels) == false
            plot_add_text(subh(sId), ylabels{sId}, 'left');
        end
        
    end
    
    % Adding figure title(if requested)
    if isempty(figtitle) == false
        suptitle(figtitle);
    end
    
    f = gcf;
     
end
