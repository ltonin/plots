function plot_set_limits(h, target, limits)
% plot_set_limits(h, target, limits)
%
% The function set x, y or both limits on given axis. If H is a valid 
% handles vector the limits are set for all the axis. TARGET can be 'x',
% 'y' or 'both' and it identified the axis to be set. LIMITS must be a
% vector with two elements (in case of TARGET equals to 'x' or 'y') or with 
% 4 elements (in case of TARGET equals to 'both'). The format of LIMITS is
% [XMIN XMAX] or [YMIN YMAX] or [XMIN XMAX YMIN YMAX]. LIMITS can also be
% set as 'minmax'. In this case, the function set the limits accordingly to
% the min and max values of all the axis provided in H.

    

    p = inputParser;
    addRequired(p, 'target', @(x) any(validatestring(x, {'x', 'y', 'both'})));   
    parse(p, target);
    
    NumAxes = length(h);
    
    if(isnumeric(limits))
        
        if(strcmpi(target, 'both') && length(limits) ~= 4)
            error('chk:lim', 'Provide 4 element vector for axis limits [XMIN XMAX YMIN YMAX]');
        end
        
        for xId = 1:NumAxes
            switch(upper(target))
                case 'X'
                    xlim(h(xId), limits);
                case 'Y'
                    ylim(h(xId), limits);
                case 'BOTH'
                    axis(h(xId), limits);
            end
        end
    elseif(ischar(limits) && strcmpi(limits, 'minmax'))
        
        xcurrlim = zeros(NumAxes, 2);
        ycurrlim = zeros(NumAxes, 2);
        for xId = 1:NumAxes
            xcurrlim(xId, :) = get(h(xId), 'XLim');
            ycurrlim(xId, :) = get(h(xId), 'YLim');
        end
        
        xlimits = [min(xcurrlim(:, 1)) max(xcurrlim(:, 2))];
        ylimits = [min(ycurrlim(:, 1)) max(ycurrlim(:, 2))];
        
        for xId = 1:NumAxes
            switch(upper(target))
                case 'X'
                    xlim(h(xId), xlimits);
                case 'Y'
                    ylim(h(xId), ylimits);
                case 'BOTH'
                    xlim(h(xId), xlimits);
                    ylim(h(xId), ylimits);
            end
        end
    end
    
end

% 
% plot_set_limits(h, 'x', [0 1])
% plot_set_limits(h, 'y', [0 1])
% plot_set_limits(h, 'both', [0 1 0 1])
% plot_set_limits(h, 'x', 'minmax')
% plot_set_limits(h, 'y', 'minmax')
% plot_set_limits(h, 'both', 'minmax')