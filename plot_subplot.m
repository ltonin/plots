function [options] = plot_subplot(x, data, varargin)
% [options] = plot_subplot(x, y, data, varargin) (NOT READY - DON'T USE - STILL ONGOING WORK)
%
% The function create a new figure with subplot with given layout.
% Input arguments:
%
%   x       vector for x-axis of the image
%   data    data matrix with 2 dimensions [y - subplot]
%
% Output:
%   options Structure with images informations
%
% Option input argument pairs:
%
% 'layout', [nrows x ncols]         Layout of the subplots. The total 
%                                   number of elements must be >= of the
%                                   number of subplots. Indexes in the
%                                   layout must range from 1 to the number
%                                   of subplots. If empty subplots are
%                                   required fill the position with 0s.
%                                   [Default layout: 5 columns, with
%                                   positions from 1 to number of subplots]
% 'subtitles' {'TITLE1'.. TITLEN}   List of subtitles. Length of subtitles 
%                                   must be equal to the number of subplots
% 'xlabel', 'label name'            Label of x-axis for each subplot.
%                                   [Default value: 'x']
% 'figtitle', 'Figure title'        Title of the whole figure. 
%                                   [Default value: 'Title: SubImagesc']
% 'xlim',   [minval maxval]         Set x-limits for each subplot. 
%                                   [Default value: [min(x) max(x)]
% 'ylim',   [minval maxval]         Set x-limits for each subplot. 
%                                   [Default value: [min(min(data))) max(max(data))]
% 'fhandle',    @func               Function handle to be applied on each
%                                   subplot.
%                                   [Default value: '']
% 'fargs',      {args}              Arguments for @func
%                                   [Default value: {}]
%
% SEE ALSO: plot, subplot, figure, util_getargs

disp('NOT READY - DONT USE - STILL ONGOING WORK');

    %% Check input arguments
    if ndims(data) > 3
        error('chk:data', 'data must at max 3 dimensions');
    end
    
    if length(x) ~= size(data, 1)
        error('chk:data', 'x vector must have same length of size(data, 1)');
    end
    
    
    options = option_arguments(size(data, 2), varargin);

    if isempty(options.xlim)
        MinValueX = min(x);
        MaxValueX = max(x);

        if(isequal(MinValueX, MaxValueX))
            MaxValueX = MinValueX + 1;
        end
        options.xlim = [MinValueX MaxValueX];
    end
    
    if isempty(options.ylim)
        MinValueY = min(min(min(data)));
        MaxValueY = max(max(max(data)));

        if(isequal(MinValueY, MaxValueY))
            MaxValueY = MinValueY + 1;
        end
        options.ylim = [MinValueY MaxValueY];
    end
    
    %% Plotting sub imagesc
    NumRows  = options.nrows;
    NumCols  = options.ncols;
    NumPlots = options.nplots;
    
    f = figure;
    set(gcf,'name',options.figtitle,'numbertitle','off')
    
    hdr = zeros(NumPlots, 1);
    for pId = 1:NumPlots
        [cx, cy] = find(options.layout == pId); 
        index = (cx-1)*NumCols + cy;
        subplot(NumRows, NumCols, index);

        if ismatrix(data)
            cdata = data(:, pId);
        else
            cdata = squeeze(data(:, pId, :));
        end

        %hdr(pId) = plot(x, cdata);
        plot(x, cdata);
        xlim(options.xlim);
        ylim(options.ylim);

        xtick = get(gca, 'XTick');

        set(gca, 'XTickLabel', num2str(x(xtick), '%1.1f'));

        title(options.subtitles{pId});

        ylabel(options.ylabel);
        xlabel(options.xlabel);
        grid on;
        
        if isempty(options.fhandle) == false
            options.fhandle(options.fargs{:});
        end
    end
    
    options.hfigure = f;
    options.hplots  = hdr;
end

function options = option_arguments(nplots, arg_options)
    dflt_ncols      = 5;
    dflt_nentries   = (ceil(nplots/dflt_ncols)*dflt_ncols);
    dflt_index      = zeros(dflt_nentries, 1);
    dflt_index(1:nplots) = 1:nplots;
    dflt_layout     = reshape(dflt_index, [dflt_ncols ceil(nplots/dflt_ncols)])';
    dflt_names      = cellfun(@num2str, num2cell(1:nplots), 'UniformOutput', 0);
    
    pnames = {  ...
                'layout',     ...
                'subtitles',      ...
                'xlabel',     ...
                'ylabel',     ...
                'figtitle',   ... 
                'xlim',       ...
                'ylim',       ...
                'fhandle',    ...
                'fargs',      ...
              };
    
    default = { ...
                dflt_layout,            ...
                dflt_names,             ...
                'x',                    ...
                'y',                    ... 
                'Title: SubPlot',       ...
                [],                     ...
                [],                     ...
                [],                     ...
                {},                     ...
                };

    [~,emsg, ...
     layout, ...
     subtitles,  ...
     x_lb,      ...
     y_lb,      ...
     figtitle,  ...
     xlim,      ... 
     ylim,      ...
     fhandle, ...
     fargs, ...
     ] = util_getargs(pnames, default, arg_options{:}); 
    
    if(isempty(emsg) == false)
        error('chk:arg', emsg);
    end
 
    nrows = size(layout, 1);
    ncols = size(layout, 2);

    if(numel(layout) < nplots)
        error('chk:arg', ['Provide layout large enough (>=' num2str(nplots) ') for the subplots. Actual layout size (' num2str(numel(layout)) ': ' num2str(nrows) 'x' num2str(ncols) ')']);
    end
    
    if(unique(layout(layout > 0)) ~= nplots)
        error('chk:arg', ['Provide a layout with indexes for each subplot (from 1 to ' num2str(nplots) '). Fill the eventual additional index with 0']);
    end
    
    if(length(subtitles) ~= nplots)
        error('chk:arg', ['Provide a title for each subplot (=' num2str(nplots) ')']);
    end
    
    options.layout      = layout;
    options.subtitles   = subtitles;
    options.xlabel      = x_lb;
    options.ylabel      = y_lb;
    options.figtitle    = figtitle;
    options.xlim        = xlim;
    options.ylim        = ylim;
    options.fhandle     = fhandle;
    options.fargs       = fargs;
    options.nrows       = size(layout, 1);
    options.ncols       = size(layout, 2);
    options.nplots      = nplots;

end
