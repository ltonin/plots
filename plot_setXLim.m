function [handles] = plot_setXLim(handles, values)
% PLOT_SETXLIM   This function set the Xlim of the argument handles to the same
%           values.
%
%           plot_setXLim(handles, values)
%                                   - handles:  the figures axis handles,
%                                               can be a vector.
%                                   - values:   min and max value in the
%                                               format [min max]  

    if ~(length(values)== 2)
        error('setAx:chk', 'The values input must be in the format [min max]');
    end
    
    if values(2) < values(1)
        error('setAx:chk', 'The values input must be in the format [min max]');
    end

    
    for hIdx = 1:length(handles)     
        xlim(handles(hIdx), values);
    end
    
end
