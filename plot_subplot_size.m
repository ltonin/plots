function [nRows, nCols] = plot_subplot_size(nplots, varargin)

 %% Input parser
    
    % Default patterns
    defaultCols = nplots;
    
    % Validation functions
    isnumber    = @(x) assert(isnumeric(x) && isscalar(x), 'Max number of columns must be a scalar');
    
    % Add parameters
    p = inputParser;
    p.addRequired('nplots', isnumber);
    p.addParameter('maxcols', defaultCols, isnumber);
    
    % Parse input
    parse(p, nplots, varargin{:});
    
    maxcols = p.Results.maxcols;
    
    if maxcols >= nplots 
        nRows = 1;
        nCols = nplots;
    else
        nCols = maxcols;
        nRows = ceil(nplots/maxcols);
    end
    
end