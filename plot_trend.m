function [h, v_i] = plot_trend(v, FormatData, FormatTrend, FitOrder)

    if nargin < 4
        FitOrder = 3;
    end
    if nargin < 3
        FitOrder = 3;
        FormatTrend = 'b';
    end
    if nargin < 2;
        FitOrder = 3;
        FormatData = 'b.';
        FormatTrend = 'b';
    end

    NumSamples = size(v, 1);
    h = zeros(2, 1);
    
    ind = 1:NumSamples;
    k = ~isnan(v);
    p = polyfit(ind(k), v(k)', FitOrder);
    v_i = polyval(p, 1:NumSamples);       

    hold on;
    h(1) = plot(ind, v, FormatData);
    h(2) = plot(ind, v_i, FormatTrend, 'LineWidth', 2);
    hold off;
    
   


end