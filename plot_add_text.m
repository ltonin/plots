function [hn] = plot_add_text(hg, note, position, padding)
% [hn] = plot_add_text(hg, note, position [, padding])
%
% The function add text to the provided graphic handler. Text can be added
% at default ('top', 'bottom', 'left', 'right') or at custom positions. 
% With the optional argument padding that set the distance frome axes.
%
%   Input arguments:
%   hg          Handler to graphic object (axes, image)
%   note        Text to be added
%   position    Possible default positions:
%               - 'top'     at the top of the axes
%               - 'bottom'  at the bottom of the axes
%               - 'left'    at the left of the axes (rotated)
%               - 'right'   at the right of the axes (rotated)
%               -  [x y]    at the custom position (x, y)
%   padding     Optional argument to set the distance of the text from the
%               axes (or from the provided position)
%
%   Output arguments:
%   hn          Handler to the text
%
    %% Check input arguments
    if ishandle(hg) == false
        error('chk:hdl', ['[' mfilename '] Provide a valid graphic handler']);
    end

    switch lower(position)
        case {'bottom', 'top', 'left', 'right'}
        otherwise
             error('chk:pos', ['[' mfilename '] Unknown position']);
    end
            
    %% Getting handler limits
    xlimits = xlim(hg);
    ylimits = ylim(hg);

    %% Initialize padding argument
    if nargin == 3
        padding = [];
    end
    
    if ischar(position)
        switch lower(position)
            case 'bottom'
                % Default padding for bottom position
                padding = 0.2*diff(ylimits);
            case 'top'
                % Default padding for top position
                padding = 0.08*diff(ylimits);
            case 'left'
                % Default padding for top position
                padding = 0.09*diff(xlimits);
            case 'right'
                % Default padding for top position
                padding = -0.02*diff(xlimits);
        end
    end
    
    if isempty(padding)
        padding = 0;
    end

    %% Correction for images (ylimits flipped and padding reversed)
    if isempty(getimage(hg)) == false
        ylimits = fliplr(ylimits);
        switch lower(position)
            case {'top', 'bottom'}
                padding = -padding;
        end
    end
    
    %% Computing x and y position
    if isnumeric(position)
        xposition = position(1);
        yposition = position(2);
        rotation  = 0;
    else 
        switch lower(position)
            case 'bottom'
                xposition = xlimits(1) + diff(xlimits) / 2; 
                yposition = ylimits(1) - padding;
                rotation  = 0;
            case 'top' 
                xposition = xlimits(1) + diff(xlimits) / 2; 
                yposition = ylimits(2) + padding;   
                rotation  = 0;
            case 'left' 
                xposition = xlimits(1) - padding; 
                yposition = ylimits(1) + diff(ylimits) /2;
                rotation  = 90;
            case 'right' 
                xposition = xlimits(2) - padding; 
                yposition = ylimits(1) + diff(ylimits) /2;
                rotation  = -90;
        end
    end
    
    %% Adding text to handler
    hn = text(xposition, yposition, note, 'Parent', hg);
    set(hn, 'HorizontalAlignment', 'center');
    set(hn, 'rotation', rotation);
end